﻿using Dash.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Dash.Data.DataContext
{
    public class DatabaseContext : DbContext
    {
        public class OptionsBuild
        {
            public OptionsBuild()
            {
                Settings = new AppConfiguration();
                OpsBuilder = new DbContextOptionsBuilder<DatabaseContext>();
                OpsBuilder.UseSqlServer(Settings.SqlConnectonString);
                DbOption = OpsBuilder.Options;
            }
            public DbContextOptionsBuilder<DatabaseContext> OpsBuilder { get; set; }
            public DbContextOptions<DatabaseContext> DbOption { get; set; }
            private AppConfiguration Settings { get; set; }
        }
        public static OptionsBuild ops = new OptionsBuild();
        public DatabaseContext(DbContextOptions<DatabaseContext> dbOption) : base(dbOption) { }

        public DbSet<User> Users { get; set; }
    }
}
