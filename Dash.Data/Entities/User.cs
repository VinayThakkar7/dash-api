﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dash.Data.Entities
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int user_id { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "First Name is required")]
        [StringLength(100, ErrorMessage = "First Name cannot be longer than 100 characters")]
        public string first_name { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Last Name is required")]
        [StringLength(100, ErrorMessage = "Last Name cannot be longer than 100 characters")]
        public string last_name { get; set; }

        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Required(ErrorMessage = "Email ID is required")]
        [StringLength(170, ErrorMessage = "Email ID cannot be longer than 70 characters")]
        public string email_Id { get; set; }


        [DataType(DataType.Password)]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z])(.{8,15})$", ErrorMessage = "Password must contain atleast 1 lower case letter, 1 uppercase letter,1 number and 1 special char..")]
        [Required(ErrorMessage = "Password is required")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        public string password { get; set; }

        [DataType(DataType.Text)]
        [StringLength(500, ErrorMessage = "Verification cannot be longer than 500 characters")]
        public string verification_code { get; set; }
    }
}
