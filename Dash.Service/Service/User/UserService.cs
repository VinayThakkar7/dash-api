﻿using Dash.Data;
using Dash.Data.DataContext;
using Dash.Data.Entities;
using Dash.Service.Helper;
using Dash.Service.Interface;
using Dash.Service.Models.User;
using System.Collections.Generic;
using System.Linq;
using static Dash.Service.Helper.ServiceResponse;

namespace Dash.Service.Service
{
    public class UserService : IUserService
    {
        public ServiceResponseGeneric<List<UserServiceModel>> Create()
        {
            throw new System.NotImplementedException();
        }

        public ServiceResponseGeneric<List<UserServiceModel>> GetAll()
        {

            throw new System.NotImplementedException();
        }

        public ServiceResponseGeneric<UserServiceModel> GetById(int id)
        {
            throw new System.NotImplementedException();
        }

        public ServiceResponseGeneric<List<UserServiceModel>> Update()
        {
            throw new System.NotImplementedException();
        }

        public ServiceResponseGeneric<List<User>> GetUser()
        {
            return new ServiceResponseGeneric<List<User>>(() =>
            {
                var users = new List<User>();
                using (var dbContext = new DatabaseContext(DatabaseContext.ops.DbOption))
                {
                    users = dbContext.Users.ToList();
                }
                var outData = TestMethod();
                if (!outData.Success)
                {
                    throw new ServiceResponseExceptionHandle(outData.Errors.First().ErrorMessage, outData.Errors.First().StatusCode);
                }
                else
                {
                    return outData.Output;
                }
            });
        }

        public ServiceResponseGeneric<List<User>> TestMethod()
        {
            return new ServiceResponseGeneric<List<User>>(() =>
            {
                var userList = new List<User>
                {
                    new User
                    {
                        user_id = 1,
                        password = "Admin@123",
                        first_name = "Demo@admin.com"
                    },
                    new User
                    {
                        user_id = 2,
                        password = "Admin@123",
                        first_name = "Test@admin.com"
                    }
                };

                return userList;
               
            });
        }

    }
}
