﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Dash.Service.Helper
{
    public class ServiceResponse
    {
        public class FileResponse
        {
            private bool? _success;
            private string fileStream;
            public FileResponse()
                : this((FileResponse)null)
            {
            }

            public FileResponse(ErrorInfo error)
                : this(new[] { error })
            {
            }


            public FileResponse(IEnumerable<ErrorInfo> errors)
                : this((FileResponse)null)
            {
                foreach (var errorInfo in errors)
                    Errors.Add(errorInfo);
            }

            public FileResponse(FileResponse result)
            {
                if (result != null)
                {
                    Success = result.Success;
                    Errors = new List<ErrorInfo>(result.Errors);
                    StatusCode = result.StatusCode;
                    FileStream = result.fileStream;
                }
                else
                {
                    Errors = new List<ErrorInfo>();
                    //Messages = new List<InfoMessage>();
                    StatusCode = result.StatusCode;
                }
            }

            /// <summary>
            ///     Indicates if result is successful.
            /// </summary>
            public bool Success
            {
                get => _success ?? Errors.Count == 0;
                set => _success = value;
            }
            public HttpStatusCode StatusCode { get; set; }

            /// <summary>
            ///     Gets a list of errors.
            /// </summary>
            public IList<ErrorInfo> Errors { get; }
            public string FileStream { get; set; }

        }

        public class ServiceResponseGeneric
        {
            private bool? _success;
        }

        public class ErrorInfo
        {
            public ErrorInfo()
               : this(HttpStatusCode.BadRequest, string.Empty)
            {
            }

            public ErrorInfo(string errorMessage)
                : this(HttpStatusCode.BadRequest, errorMessage)
            {
            }

            public ErrorInfo(HttpStatusCode statusCode, string errorMessage)
            {
                StatusCode = statusCode;
                ErrorMessage = errorMessage;
            }

            public HttpStatusCode StatusCode { get; set; } = HttpStatusCode.BadRequest;

            public string ErrorMessage { get; set; }

            public override string ToString()
            {
                return string.Format("{0}. Key: '{1}', ErrorMessage: '{2}'", base.ToString(), StatusCode, ErrorMessage);
            }
        }

        public class ServiceResponseGeneric<T>
        {
            private bool? _success;
            public bool Success
            {
                get => _success ?? Errors.Count == 0;
                set => _success = value;
            }
            public HttpStatusCode StatusCode { get; set; }
            public List<ErrorInfo> Errors { get; } = new List<ErrorInfo>();
            public ServiceResponseGeneric(string msg, HttpStatusCode httpStatusCode)
            {
                StatusCode = httpStatusCode;
                Errors.Add(new ErrorInfo
                {
                    ErrorMessage = msg,
                    StatusCode = httpStatusCode
                });
            }
            public ServiceResponseGeneric(Func<T> result)
            {
                try
                {
                    Output = result.Invoke();
                }
                catch (ServiceResponseExceptionHandle ex)
                {
                    Errors.Add(new ErrorInfo { ErrorMessage = ex.Exception, StatusCode = ex.HttpStatusCode });
                }
                catch (Exception ex)
                {
                    Log.Error($"StackTrace = {ex.StackTrace}, " +
                               $"Message = {ex.Message}, " +
                                $" InnerException = {ex.InnerException}, " +
                                $" ex.InnerException.Message = {ex.InnerException?.Message} ");
                    Errors.Add(new ErrorInfo { ErrorMessage = ex.Message, StatusCode = HttpStatusCode.BadRequest });
                }
            }
            

            public T Output { get; set; }
        }


        public class ServiceResponseExceptionHandle : Exception
        {
            public string Exception { get; set; }
            public HttpStatusCode HttpStatusCode { get; set; }
            public ServiceResponseExceptionHandle()
            {

            }
            public ServiceResponseExceptionHandle(string msg, HttpStatusCode httpStatusCode)
            {
                Exception = msg;
                HttpStatusCode = httpStatusCode;
            }

        }
    }
}
