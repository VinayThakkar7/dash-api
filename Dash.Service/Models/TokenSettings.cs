﻿namespace Dash.Service.Models
{
    public class TokenSettings
    {
        public string securityKey { get; set; }

        public string validIssuer { get; set; }

        public string validAudience { get; set; }

        public double expiryInMinutes { get; set; }
    }
}
