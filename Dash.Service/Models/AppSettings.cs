﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dash.Service.Models
{
    public class AppSettings
    {
        public TokenSettings TokenSettings { get; set; }
       
        public string PortalUrl { get; set; }

    }
}
