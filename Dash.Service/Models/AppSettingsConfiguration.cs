﻿namespace Dash.Service.Models
{
    public class AppSettingsConfiguration
    {
        public string AccountSid { get; set; }
        public string ApiKey { get; set; }
        public string ApiSecret { get; set; }
    }
}
