﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dash.Service.Models.User
{
    public class UserServiceModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
