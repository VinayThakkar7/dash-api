﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dash.Service.Models.User
{
    public class UserAuthenticaionResponseModel
    {
        public string Token { get; set; }

        public string UserID { get; set; }
        public string EmailID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
