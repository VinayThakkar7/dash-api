﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Dash.Data.Entities;
using Dash.Service.Models.User;

namespace Dash.Service
{
    public class AutoMapperConfiguration : Profile
    {
        public AutoMapperConfiguration()
        {
            CreateMap<User, UserServiceModel>().ReverseMap();
        }
    }
}
