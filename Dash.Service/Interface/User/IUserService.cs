﻿using Dash.Data.Entities;
using System.Collections.Generic;
using static Dash.Service.Helper.ServiceResponse;
using Dash.Service.Models.User;

namespace Dash.Service.Interface
{
    public interface IUserService : ICRUDInterface<UserServiceModel>
    {
        ServiceResponseGeneric<List<User>> GetUser();
    }
}
