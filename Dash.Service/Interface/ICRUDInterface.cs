﻿using System.Collections.Generic;
using static Dash.Service.Helper.ServiceResponse;

namespace Dash.Service.Interface
{
    public interface ICRUDInterface<T>
    {
        ServiceResponseGeneric<List<T>> Create();
        ServiceResponseGeneric<List<T>> Update();
        ServiceResponseGeneric<T> GetById(int id);
        ServiceResponseGeneric<List<T>> GetAll();
    }
}
