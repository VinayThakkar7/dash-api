﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Dash.Service.Helper.ServiceResponse;

namespace Dash.WebApi.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v1/[controller]")]
    public class BaseController : ControllerBase
    {
        public IActionResult GenerateResponse<T>(ServiceResponseGeneric<T> serviceResponse)
        {
            if (serviceResponse.Success)
            {
                return Ok(serviceResponse.Output);
            }
            return BadRequest(serviceResponse.Errors);
        }
    }
}
