﻿using Dash.Service.Interface;
using Dash.Service.Models;
using Dash.Service.Models.User;
using Dash.WebApi.JwtFeatures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dash.WebApi.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IOptions<AppSettings> _appSettings;
        private readonly JwtHandler _jwtHandler;
        private readonly IUserService _userService;
        public AccountController(IOptions<AppSettings> appSettings, IUserService userService, JwtHandler jwtHandler)
        {
            _jwtHandler = jwtHandler;
            _userService = userService;
            _appSettings = appSettings;
        }
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate(int UserID)
        {
            
            try
            {
                var authenticatedUser = _userService.GetById(UserID);

                if (authenticatedUser != null)
                {
                    UserAuthenticaionResponseModel response = new UserAuthenticaionResponseModel
                    {                       
                        //Token = _jwtHandler.GenerateTokenOptions(authenticatedUser),
                    };
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
            return Ok();
        }
    }
}
