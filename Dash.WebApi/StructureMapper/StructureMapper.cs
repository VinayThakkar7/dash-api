﻿using Dash.Service.Interface;
using Dash.Service.Service;
using Microsoft.Extensions.DependencyInjection;

namespace Dash.WebApi
{
    public static class StructureMapper
    {
        public static void InitializeStructureMapper(IServiceCollection services)
        {
            services.AddScoped<IUserService, UserService>();
        }
    }
}
