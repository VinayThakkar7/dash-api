﻿using Dash.Data.Entities;
using Dash.Service.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Dash.WebApi.JwtFeatures
{
    public class TokenService
    {
        private const string PermissionSeparator = ",";
        private readonly AppSettings appSettings;
        private readonly JwtSecurityTokenHandler jwtSecurityTokenHandler;

        public TokenService(IOptions<AppSettings> appSettings)
        {
            this.appSettings = appSettings.Value;
            jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
        }

        public string GenerateTokenOptions(User user)
        {
            var expiry = DateTime.UtcNow.AddMinutes(appSettings.TokenSettings.expiryInMinutes);
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(appSettings.TokenSettings.securityKey));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var jwtToken = new JwtSecurityToken(
                appSettings.TokenSettings.validIssuer,
                appSettings.TokenSettings.validAudience,
                GetClaims(user),
                expires: expiry,
                signingCredentials: credentials);
            return jwtSecurityTokenHandler.WriteToken(jwtToken);
        }

        private List<Claim> GetClaims(User user)
        {
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(nameof(User.first_name), user.first_name));
            claims.Add(new Claim(nameof(User.last_name), user.last_name));
            claims.Add(new Claim(nameof(User.email_Id), user.email_Id));
            claims.Add(new Claim(nameof(User.user_id), user.user_id.ToString()));

            return claims;
        }
    }
}
